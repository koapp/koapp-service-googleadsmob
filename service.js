(function () {
  angular
    .module('king.services.googleadmob', [])
    .run(googleadmobService);

  googleadmobService.$inject = ['configService'];

  function googleadmobService(configService) {

    if (configService.services && configService.services.googleadmob) {
      googleadmobFunction(configService.services.googleadmob.scope);
    }

    function googleadmobFunction(scopeData) {

      try {
        if(configService || navigator || AdMob){
            let admobScope = configService.services.googleadmob.scope;
            // select the right Ad Id according to platform
          var admobid = {};
          if( /(android)/i.test(navigator.userAgent) ) { // for android & amazon-fireos
            admobid = {
              banner: admobScope.testing ? 'ca-app-pub-3940256099942544/6300978111': admobScope.androidBannerID, // or DFP format "/6253334/dfp_example_ad"
              interstitial: admobScope.testing ? 'ca-app-pub-3940256099942544/1033173712' : admobScope.androidInterstitialID
            };
          } else { // for ios
            admobid = {
              banner: admobScope.testing ? 'ca-app-pub-3940256099942544/6300978111': admobScope.ipodIphoneIpadBannerID, // or DFP format "/6253334/dfp_example_ad"
              interstitial: admobScope.testing ? 'ca-app-pub-3940256099942544/1033173712' : admobScope.ipodIphoneIpadInterstitialID
            };
          }
          
              // it will display smart banner at top center, using the default options
            if(AdMob) AdMob.createBanner({
              adId: admobid.banner,
              position: AdMob.AD_POSITION.BOTTOM_CENTER,
              autoShow: true });
    
            // Function to be executed when the URL changes
            function urlChanged(newUrl) {
                 AdMob.prepareInterstitial( {adId:admobid.interstitial, autoShow:false} );
                 AdMob.showInterstitial();
            }
            
            // Listen for changes in the URL using the hashchange event
            window.addEventListener("hashchange", function() {
                const newUrl = window.location.hash;
                urlChanged(newUrl);
            });
            
            // Alternatively, you can use the onpopstate event to detect changes
            // when the user navigates forward or backward in their history
            window.onpopstate = function(event) {
                const newUrl = window.location.href;
                urlChanged(newUrl);
            };
            
              // preppare and load ad resource in background, e.g. at begining of game level
            //if(AdMob) AdMob.prepareInterstitial( {adId:admobid.interstitial, autoShow:false} );
            
            // show the interstitial later, e.g. at end of game level
            //if(AdMob) AdMob.showInterstitial();
        }
        } catch (error) {
            console.log("adds plugin not found");
            console.error(error);
        }

      var admobid = {};

      if ((/(ipad|iphone|ipod|android)/i.test(navigator.userAgent))) {
        if (/(android)/i.test(navigator.userAgent)) {
          admobid = { // for Android
            banner: scopeData.androidBannerID || false,
            interstitial: scopeData.androidInterstitialID || false,
            appOpen: scopeData.androidAppOpenID || false,
            rewarded: scopeData.androidRewardedID || false
            //native : scopeData.androidNativeID || false,
          };
        } else if (/(ipod|iphone|ipad)/i.test(navigator.userAgent)) {
          admobid = { // for iOS
            banner: scopeData.ipodIphoneIpadBannerID || false,
            interstitial: scopeData.ipodIphoneIpadInterstitialID || false,
            appOpen: scopeData.ipodIphoneIpadAppOpenID || false,
            rewarded: scopeData.ipodIphoneIpadRewardedID || false
            //native : scopeData.ipodIphoneIpadNativeID || false,
          };
        }

        document.addEventListener('deviceready', async () => {

          if (scopeData.useBanner && admobid.banner) {
            let banner;

            document.addEventListener('deviceready', async () => {
              banner = new admob.BannerAd({
                adUnitId: scopeData.testing ? 'ca-app-pub-3940256099942544/6300978111' : admobid.banner,
                position: scopeData.bannerTop ? 'top' : 'bottom',
                offset: scopeData.bannerOffset === -1? undefined : scopeData.bannerOffset
              })

              admob.BannerAd.config({
                backgroundColor: scopeData.bannerBackgroundColor === ""? undefined: scopeData.bannerBackgroundColor,
                marginTop: scopeData.bannerMarginTop === -1? undefined: scopeData.bannerMarginTop,
                marginBottom: scopeData.bannerMarginBottom === -1? undefined: scopeData.bannerMarginBottom
              })


              banner.on('impression', async (evt) => {
                //await banner.hide()
              })

              await banner.show();
            }, false)
          }

          if (scopeData.useIntersitial && admobid.interstitial) {
            let interstitial;

            document.addEventListener('deviceready', async () => {
              interstitial = new admob.InterstitialAd({
                adUnitId: scopeData.testing ? 'ca-app-pub-3940256099942544/1033173712' : admobid.interstitial
              })

              interstitial.on('load', (evt) => {
                // evt.ad
              })

              await interstitial.load();
              await interstitial.show();
            }, false)

            document.addEventListener('admob.ad.dismiss', async () => {
              await interstitial.load();
            })
          }

          if (scopeData.useAppOpen && admobid.appOpen) {
            let appOpen;

            document.addEventListener('deviceready', async () => {
              appOpen = new admob.AppOpenAd({
                adUnitId: scopeData.testing ? 'ca-app-pub-3940256099942544/3419835294' : admobid.appOpen,
              })

              appOpen.on('load', (evt) => {
                // evt.ad
              })

              document.addEventListener(
                'resume',
                async () => {
                  // NOTE `resume` event is triggered when dismissing interstitial ads or by other reasons,
                  // make sure to add logic to control when to display the ad.
                  if (!await appOpen.show()) {
                    await appOpen.load()
                  }
                },
                false,
              )
            }, false)

            document.addEventListener('admob.ad.show', async (evt) => {
              if (evt.appOpen instanceof admob.AppOpenAd) {
                // handle event here
              }
            })
          }

          if (scopeData.useRewarded && admobid.rewarded) {
            let rewarded

            document.addEventListener('deviceready', async () => {
              rewarded = new admob.RewardedAd({
                adUnitId: scopeData.testing ? 'ca-app-pub-3940256099942544/5224354917' : admobid.rewarded,
              })

              rewarded.on('load', (evt) => {
                // Once a rewarded ad is shown, it cannot be shown again.
                // Starts loading the next rewarded ad as soon as it is dismissed.
                async () => {
                  console.log('rewarded load');
                  await rewarded.load();
                }
              })

              await rewarded.load()
              await rewarded.show()
            }, false)
          }

          /* Error in getBoundingClientRect, admob.js
          if(){
  
           let native;
           document.addEventListener('deviceready', async () => {
              native = new admob.NativeAd({
               adUnitId: scopeData.testing ?  'ca-app-pub-3940256099942544/2247696110' : admobid.native,
             })
             native.showWith(document.getElementById('native-ad'));
             native.on('load', (evt) => {
               // evt.ad
               console.log('en native on');
             })
           
             await native.load();
             await native.show();
           })
         
 
          }*/

        }, false)
      }
    }
  }
})();
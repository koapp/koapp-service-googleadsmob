
# Documentation:
### Setting:

  - Open your [AdMob portal] account (http://www.admob.com/)
  - Add an app in the ** Applications ** option
  - Access the All applications section (* Applications / See all .. *)
  - Copy the id of the app you just created and fill in the ** Admob android app id ** field for this service.
  - Create the ad units you want to use (Banner or interstitial)
  - Copy the id of each block in the corresponding field of this service.
  - Do not forget to try your app using the test mode and ** remember to disable it when you want to publish a version **.
  -

### Changes of the new version

  1. Added support with firebase to avoid compilation errors.
  2. Added option to perform tests.
  3. Now you need to add the app id in Admob
  4. Problems solved.

# Documentación: 
### Configuración:

 - Abre tu cuenta de [AdMob portal](http://www.admob.com/)
 - Agrega una app en la opción **Aplicaciones**
 - Accede a la sección Todas las aplicaciones (*Aplicaciones/Ver todas..*)
 - Copia el id de la app que acabas de crear y rellena el campo **Admob android app id** de éste servicio.
 - Crea los bloques de anuncios que desees utilizar (Banner o interstitial)
 - Copia el id de cada bloque en el campo correspondiente de éste servicio.
 - No olvides probar tu app usando el modo de testeo y **recuerda desactivarlo cuando quieras publicar una versión**.
 - 

### Cambios de la nueva versión

 1. Agregado soporte con firebase para evitar errores de compilación
 2. Agregada opción para realizar pruebas.
 3. Ahora es necesario agregar el id de la app en Admob
 4. Problemas solucionados.


  
